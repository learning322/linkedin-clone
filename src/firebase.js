import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth, GoogleAuthProvider } from 'firebase/auth'
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyBJUsl5JynHNR1OfmOWITGlll9sJp-JNNA",
    authDomain: "linkedin-clone-c60b7.firebaseapp.com",
    projectId: "linkedin-clone-c60b7",
    storageBucket: "linkedin-clone-c60b7.appspot.com",
    messagingSenderId: "494601390574",
    appId: "1:494601390574:web:6b4c961b927941fe7b66ff"
};

const firebaseApp = initializeApp( firebaseConfig )
const db = getFirestore( firebaseApp );
const auth = getAuth()
const provider = new GoogleAuthProvider()
const storage = getStorage( firebaseApp )


export { auth, provider, storage }
export default db