import db, { auth, provider, storage } from '../firebase'

import { signInWithPopup, onAuthStateChanged, signOut } from "@firebase/auth"
import { SET_USER, SET_LOADING_STATUS, GET_ARTICLES } from './actionType'
import { addDoc, collection, onSnapshot, orderBy, query } from '@firebase/firestore'
import { getDownloadURL, ref, uploadBytesResumable } from '@firebase/storage'

export const setUser = payload => ({
    type: SET_USER,
    user: payload,
})


export const setLoading = status => ({
    type: SET_LOADING_STATUS,
    status: status,
})

export const getArticles = payload => ({

    type: GET_ARTICLES,
    payload: payload,

})

export const signInAPI = () => {

    return dispatch => {
        signInWithPopup( auth, provider )
            .then( payload => {
                dispatch( setUser(payload.user) )
            } )
            .catch( error => alert(error.message))
    }

}

export const getUserAuth = () => {
    return dispatch => {
        onAuthStateChanged( auth, user => {
            dispatch( setUser( user ) )
        } )
    }
}

export const signOutAPI = () => {
    
    return dispatch => {
        signOut( auth ).then( () => {
            dispatch( setUser(null) )
        }).catch( error => {
            alert(error.message)
            console.log(error.message)
        })
    }

}

export const postArticleAPI = payload => {

    return dispatch => {

        dispatch( setLoading(true) )

        if ( payload.image !== '' ) {

            const imageRef = ref( storage, `/images/${payload.image.name}` )
            
            const uploadTask = uploadBytesResumable( imageRef, payload.image)

            uploadTask.on('state_changed', 
                snapshot => {
                    const progress = (
                        (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    )

                    console.log(`Progress: ${progress}%`)

                    if ( snapshot.state === 'RUNNING' ) {
                        console.log(`Progress: ${progress}% 🚀🚀🚀`)
                    }

            }, error => console.log(error.code),
            () => {
                
                getDownloadURL( uploadTask.snapshot.ref ).then( downloadURL => {

                    addDoc( collection( db, "articles"), {
                        actor: {
                            description: payload.user.email,
                            title: payload.user.displayName,
                            date: payload.timestamp,
                            image: payload.user.photoURL,
                        },
                        // image: payload.image,
                        video: payload.video,
                        sharedImg: downloadURL,
                        comments: 0,
                        description: payload.description,
                    })
                    
                    dispatch( setLoading(false) )
                    
                })

            })

        } else if ( payload.video !== '' ) {

            addDoc( collection( db, "articles"), {
                actor: {
                    description: payload.user.email,
                    title: payload.user.displayName,
                    date: payload.timestamp,
                    image: payload.user.photoURL,
                },
                video: payload.video,
                sharedImg: "",
                comments: 0,
                description: payload.description,

            })

            dispatch( setLoading(false) )

        } else {

            addDoc( collection( db, "articles"), {
                actor: {
                    description: payload.user.email,
                    title: payload.user.displayName,
                    date: payload.timestamp,
                    image: payload.user.photoURL,
                },
                video: "",
                sharedImg: "",
                comments: 0,
                description: payload.description,

            })

            dispatch( setLoading(false) )
        }
    }

}


export const getArticlesAPI = () => {
    return (dispatch) => {

        let payload

        const q = query( collection( db, "articles" ), orderBy("actor.date", "desc") )

        const unsubscribe = onSnapshot( q, ( querySnapshot => {

            payload = querySnapshot.docs.map( doc => doc.data() )
            dispatch( getArticles(payload) )

        }))

        return unsubscribe

    }
}
